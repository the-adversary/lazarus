'use strict';

angular.module('angularRestfulAuth')
    .factory('Main', ['$http', '$localStorage', function($http, $localStorage){
        var baseUrl = "api";
        return {
            signin: function(data, success, error) {
                $http.post(baseUrl + '/login', data).success(success).error(error)
            },
            signup: function(data, success, error) {
                $http.post(baseUrl + '/addUser', data).success(success).error(error)
            },
            postComplain: function(data, success, error) {
                $http.post(baseUrl + '/postComplain', data).success(success).error(error)
            },
            vote: function(data, success, error) {
                $http.post(baseUrl + '/vote', data).success(success).error(error)
            },
            resolve: function(data, success, error) {
                $http.post(baseUrl + '/resolve', data).success(success).error(error)
            },
            postComment: function(data, success, error) {
                $http.post(baseUrl + '/postComment', data).success(success).error(error)
            },
            me: function(success, error) {
                $http.get(baseUrl + '/me').success(success).error(error)
            },
            insti: function(success, error) {
                $http.get(baseUrl + '/insti/fresh').success(success).error(error)
            },
            hostel: function(success, error) {
                $http.get(baseUrl + '/hostel/fresh').success(success).error(error)
            }
        };
    }
]);