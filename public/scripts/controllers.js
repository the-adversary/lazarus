'use strict';

/* Controllers */

angular.module('angularRestfulAuth')
    .controller('HomeCtrl', ['$rootScope', '$scope', '$location', '$localStorage', 'Main', function($rootScope, $scope, $location, $localStorage, Main) {

        $scope.token = $localStorage.token;

        $scope.signin = function() {
            var formData = {
                entryno: $scope.email,
                password: $scope.password
            }

            Main.signin(formData, function(res) {
                console.log(res.success)
                if (res.success == false) {
                 Materialize.toast(res.message, 4000)     
                } else {
                    Materialize.toast('Welcome '+res.firstName, 1000,'',function(){window.location = '/';
                })
                    $localStorage.token = res.token;
                    $scope.token = res.token;
                    console.log(res);
                    $localStorage.firstName = res.firstName;
                    $localStorage.lastName= res.lastName;
                    $localStorage.dpUrl = res.dpUrl;
                    // location.reload();
                    // window.location = "/";    
                }
            }, function() {
                $rootScope.error = 'Failed to signin';
            })
        };
       $scope.signup = function() {
            console.log('mememew');
            var formData = {
                firstName: $scope.firstName,
                lastName: $scope.lastName,
                entryno: $scope.entryno,
                password: $scope.password,
                userType:$scope.userType,  
                location:$scope.hostel
            }
            console.log(JSON.stringify(formData));
            Main.signup(formData, function(res) {
                console.log(res.success)
                 Materialize.toast(res.message, 4000)     
            }, function() {
                $rootScope.error = 'Failed to signin';
            })
        };
        $scope.logout = function() {
            $scope.token = ''
            $localStorage.token = '' 
            // window.location = "/"    
        };
        $scope.postComplain = function() {
            var formData = {
                title: $scope.title,
                description: $scope.description,
                level:$scope.visibility,
                tags:$scope.tags
            }
            console.log(JSON.stringify(formData));
            Main.postComplain(formData, function(res) {
                console.log(res.success)
                if (res.success == false) {
                    alert(res.message)    
                } else {
                    alert('success')    
                }
            }, function() {
                $rootScope.error = 'Failed to signin';
            })
        };


    }])
           

.controller('MeCtrl', ['$rootScope', '$scope', '$location', 'Main', function($rootScope, $scope, $location, Main) {
         Main.insti(function(res) {
                $scope.insti = res;
            }, function() {
                $rootScope.error = 'Failed to fetch details';
            })
        Main.hostel(function(res) {
                $scope.hostel = res;
            }, function() {
                $rootScope.error = 'Failed to fetch details';
            })

        Main.me(function(res) {
            $scope.myDetails = res;
        }, function() {
            $rootScope.error = 'Failed to fetch details';
        })

         $scope.upvote = function(id,index,cat) {
                    var formData = {
                        id: id,
                        vote:1
                    }
                    console.log(formData);
                    Main.vote(formData, function(res) {
                        console.log(res.success)
                        if (res.success == false) {
                            // alert(res.message)    
                        } else {
                            if(cat==1) {
                                $scope.hostel[index].upvotes = res.upvotes;
                                $scope.hostel[index].downvotes = res.downvotes;
                            }else {
                                $scope.insti[index].upvotes = res.upvotes;
                                $scope.insti[index].downvotes = res.downvotes;
                            }
                            // $localStorage.token = res.token;
                            // $scope.token = res.token;
                            // window.location = "/";    
                        }
                    }, function() {
                        $rootScope.error = 'Failed to fetch details';
                    })
                };
        $scope.downvote = function(id,index,cat) {
                    var formData = {
                        id: id,
                    }
                    console.log(formData);
                    Main.vote(formData, function(res) {
                        console.log(res.success)
                        if (res.success == false) {
                            // alert(res.message)    
                        } else {
                            // $localStorage.token = res.token;
                            if(cat==1) {
                                $scope.hostel[index].upvotes = res.upvotes;
                                $scope.hostel[index].downvotes = res.downvotes;
                            }
                            else{
                                $scope.insti[index].upvotes = res.upvotes;
                                $scope.insti[index].downvotes = res.downvotes;
                            }
                            // $scope.token = res.token;
                            // window.location = "/";    
                        }
                    }, function() {
                        $rootScope.error = 'Failed to fetch details';
                    })
                };

        $scope.postComment = function(id,body,index,cat) {
                    console.log(body)
                    console.log(id);
                    var formData = {
                        id: id,
                        body:body
                    }
                            
                    Main.postComment(formData, function(res) {
                        if (res.success == true) {
                            var comment = {
                                    firstName:$scope.myDetails.firstName,
                                    lastName:$scope.myDetails.lastName,
                                    dpUrl:$scope.myDetails.dpUrl,
                                    body:body
                                }
                            // console.log(comment);
                            // console.log(index+' '+cat);
                        if(cat==1) 
                                $scope.hostel[index].comments.push(comment);
                        else 
                                $scope.insti[index].comments.push(comment);

                        } else {
                        }
                    }, function() {
                        $rootScope.error = 'Failed to fetch details';
                    })
                };
    $scope.resolve = function(id) {
        var formData = {
            id: id
        }
        console.log(formData);
        Main.resolve(formData, function(res) {
                 Materialize.toast(res.message, 4000)     
        }, function() {
            $rootScope.error = 'Failed to fetch details';
        })
    };
}]);
