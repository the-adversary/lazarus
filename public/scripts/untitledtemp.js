'use strict';

/* Controllers */

angular.module('angularRestfulAuth')
    .controller('HomeCtrl', ['$rootScope', '$scope', '$location', '$localStorage', 'Main', function($rootScope, $scope, $location, $localStorage, Main) {

        $scope.token = $localStorage.token;

        $scope.signin = function() {
            var formData = {
                entryno: $scope.email,
                password: $scope.password
            }

            Main.signin(formData, function(res) {
                console.log(res.success)
                if (res.success == false) {
                    alert(res.message)    
                } else {
                    $localStorage.token = res.token;
                    $scope.token = res.token;
                    $localStorage.firstName = res.firstName;
                    $localStorage.lastName= res.lastName;
                    $localStorage.dpUrl = res.dpUrl;
                    // window.location = "/";    
                }
            }, function() {
                $rootScope.error = 'Failed to signin';
            })
        };
        
        $scope.signup = function() {
            var formData = {
                firstName: $scope.firstName,
                lastName: $scope.lastName,
                entryno: $scope.entryno,
                password: $scope.password,
                userType:$scope.userType,  
                location:$scope.hostel
            }
            console.log(JSON.stringify(formData));
            Main.signup(formData, function(res) {
                console.log(res.success)
                if (res.success == false) {
                    alert(res.message)    
                } else {
                    alert('success')    
                }
            }, function() {
                $rootScope.error = 'Failed to signin';
            })
        };
        
        $scope.postComplain = function() {
            var formData = {
                title: $scope.title,
                description: $scope.description,
                level:$scope.visibility,
                tags:$scope.tags
            }
            console.log(JSON.stringify(formData));
            Main.postComplain(formData, function(res) {
                console.log(res.success)
                if (res.success == false) {
                    alert(res.message)    
                } else {
                    alert('success')    
                }
            }, function() {
                $rootScope.error = 'Failed to signin';
            })
        };

        Main.insti(function(res) {
                $scope.insti = res;
            }, function() {
                $rootScope.error = 'Failed to fetch details';
            })
        Main.hostel(function(res) {
                $scope.hostel = res;
            }, function() {
                $rootScope.error = 'Failed to fetch details';
            })

        $scope.resolve = function(id) {
                    var formData = {
                        id: id
                    }
                    console.log(formData);
                    Main.resolve(formData, function(res) {
                            alert(res.message)    
                    }, function() {
                        $rootScope.error = 'Failed to fetch details';
                    })
                };

        $scope.postComment = function(id,body,cat) {
                    var formData = {
                        id: id,
                        body:body
                    }
                    // console.log(formData);
                    Main.postComment(formData, function(res) {
                        if(res.success)
                        {
                            if(cat==1) {
                                var comment = {
                                    firstName:$localStorage.firstName,
                                    lastName:$localStorage.lastName,
                                    dpUrl:$localStorage.dpUrl,
                                    body:body
                                }
                                console.log();
                                $scope.hostel[index].comments.push(comment);
                            }
                        }
                    }, function() {
                        $rootScope.error = 'Failed to fetch details';
                    })
                };
        

        $scope.logout = function() {
            $scope.token = ''
            $localStorage.token = '' 
            // window.location = "/"    
        };

        $scope.upvote = function(id,index,cat) {
                    var formData = {
                        id: id,
                        vote:1
                    }
                    console.log(formData);
                    Main.vote(formData, function(res) {
                        console.log(res.success)
                        if (res.success == false) {
                            // alert(res.message)    
                        } else {
                            if(cat==1) {
                                $scope.hostel[index].upvotes = res.upvotes;
                                $scope.hostel[index].downvotes = res.downvotes;
                            }
                            // $localStorage.token = res.token;
                            // $scope.token = res.token;
                            // window.location = "/";    
                        }
                    }, function() {
                        $rootScope.error = 'Failed to fetch details';
                    })
                };
        $scope.downvote = function(id,index,cat) {
                    var formData = {
                        id: id,
                    }
                    console.log(formData);
                    Main.vote(formData, function(res) {
                        console.log(res.success)
                        if (res.success == false) {
                            // alert(res.message)    
                        } else {
                            // $localStorage.token = res.token;
                            if(cat==1) {
                                $scope.hostel[index].upvotes = res.upvotes;
                                $scope.hostel[index].downvotes = res.downvotes;
                            }
                            // $scope.token = res.token;
                            // window.location = "/";    
                        }
                    }, function() {
                        $rootScope.error = 'Failed to fetch details';
                    })
                };
    }])

.controller('MeCtrl', ['$rootScope', '$scope', '$location', 'Main', function($rootScope, $scope, $location, Main) {

        Main.me(function(res) {
            $scope.myDetails = res;
        }, function() {
            $rootScope.error = 'Failed to fetch details';
        })
}]);
