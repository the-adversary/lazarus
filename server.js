// =======================
// get the packages we need ============
// =======================
var express     = require('express');
var app         = express();
var bodyParser  = require('body-parser');
var morgan      = require('morgan');
var mongoose    = require('mongoose');
var fs    = require('fs');
var multer  = require('multer')
var upload = multer({ dest: 'uploads/dp/' })
var uploadComp = multer({dest: 'uploads/complaints/'})

var jwt    = require('jsonwebtoken'); // used to create, sign, and verify tokens
var config = require('./app/config'); // get our config file
var Models   = require('./app/model'); // get our mongoose model
    
// =======================
// configuration =========
// =======================
var port = process.env.PORT || 8080; // used to create, sign, and verify tokens
mongoose.connect(config.database); // connect to database
app.set('superSecret', config.secret); // secret variable

// use body parser so we can get info from POST and/or URL parameters
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

// use morgan to log requests to the console
app.use(morgan('dev'));
// =======================
// routes ================
// =======================
app.use(express.static(__dirname + '/public'));
// basic route
app.get('/', function(req, res) {
res.sendfile('./public/index.html'); // load the single view file (angular will handle the page changes on the front-end)
});


// API ROUTES -------------------
// we'll get to these in a second

// =======================
// start the server ======
// =======================
app.listen(8080);
console.log('Server at http://localhost:' + port);
console.log(Date.now()/1000);
// Our data types
var User  = Models.User;
var Complain  = Models.Complain;
var Comment  = Models.Comment;
var Notification  = Models.Notification;

// ROUTES FOR OUR API
// =============================================================================

// middleware to use for all requests

// API ROUTES -------------------

// get an instance of the router for api routes
var apiRoutes = express.Router(); 

// To populate with a single admin
// These routes are oublic 
apiRoutes.get('/', function(req, res) {
  var user = new User();
  user.firstName = 'Saurabh';
  user.lastName = 'Kumar';
  user.entryno = 'cs1140252';
  user.location = 'Kumaon';
  user.password = 'mew';
  user.userType = 0;
  user.dpUrl = 'admin';
  // save the super user

  user.save();

    res.send('Hello! The API is at http://localhost:' + port + '/api');

});

apiRoutes.get('/users', function(req, res) {
  User.find({}, function(err, users) {
    res.json(users);
  });
});


apiRoutes.post('/login' ,function(req, res) {

  // find the user
  User.findOne({
    entryno: req.body.entryno
  }, function(err, user) {

    if (err) throw err;

    if (!user) {
      res.json({ success: false, message: 'Authentication failed. User not found.' });
    } else if (user) {

      // check if password matches
      if (user.password != req.body.password) {
        res.json({ success: false, message: 'Authentication failed. Wrong password.' });
      } else {

        // if user is found and password is right
        // create a token
        var token = jwt.sign(user, app.get('superSecret'), {
          expiresIn: "7d" // expires in 7 days
        });

        // return the information including token as JSON
        res.json({
          success: true,
          message: 'Enjoy your token!',
          token: token,
          firstName : user.firstName,
		  lastName : user.lastName,
          dpUrl : user.dpUrl,
          userType : user.userType,
          location : user.location
        });
      }   

    }

  });
});

apiRoutes.get('/uploads/dp/:file_id', function (req, res) {    
     var img = fs.readFileSync(__dirname+'/uploads/dp/'+req.params.file_id);
     res.writeHead(200, {'Content-Type': 'image/jpeg' });
     res.end(img, 'binary');
});

// route to Authenticationhenticate a user (POST http://localhost:8080/api/authenticate)
// route middleware to verify a token

apiRoutes.use(function(req, res, next) {
  // check header or url parameters or post parameters for token
  var token = req.body.token || req.query.token || req.headers['authorization'];
  // decode token
  if (token) {

    // verifies secret and checks exp
    jwt.verify(token, app.get('superSecret'), function(err, decoded) {      
      if (err) {
        return res.json({ success: false, message: 'Failed to authenticate token.' });    
      } else {
        // if everything is good, save to request for use in other routes
        req.decoded = decoded;    
        next();
      }
    });

  } else {

    // if there is no token
    // return an error
    return res.status(403).send({ 
        success: false, 
        message: 'No token provided.' 
    });
    
  }
});

// route for updating profile picture
apiRoutes.post('/updateDP', upload.single('avatar'),function(req, res) {
if(!req.file) {return res.status(403).send({ 
        success: false, 
        message: 'no dp.' 
    });}


User.findOneAndUpdate({ dpUrl: req.decoded._doc.dpUrl }, { dpUrl: req.file.filename }, function(err, user) {
  if (err) throw err;
return res.status(200).send({ 
        success: true, 
        dpUrl:req.file.filename,
        message: 'updated dp' 
    });
});
});

// route for adding a user to the db
apiRoutes.post('/addUser', upload.single('avatar'),function(req, res) {
// if a superuser posted this request
if (req.decoded._doc.userType == 0) {
  var user = new User(req.body);
  if(!req.file) user.dpUrl='default';
  else user.dpUrl = req.file.filename;
  user.userType = parseInt(req.body.userType);
  // Faculty and Student user type are kept same for now
  if(user.userType==2) user.userType=1;
  // these are resolvers
  if(user.userType == 3 || user.userType == 4 || user.userType == 5) {
  user.compResolved=0;
  user.compReceived=0;
  user.avTime=0;
  // save the user
  User.findOne({
    userType: user.userType
        }, function(err, resolver) {
            if(resolver) {            
			    res.json({ success: false,message:'resolver already exists'});
            }
            else
            	user.save(function(err) {
			    if (err) return res.json(err);
			    res.json({ success: true });
			  });
        });
  }
  else user.save(function(err) {
    if (err) return res.json(err);

    console.log('User saved successfully');
    res.json({ success: true });
  });
  } else {
  	// not a superuser request
  	// not authorized
    return res.send({ 
        success: false, 
        message: 'not authorized.' 
    });
    
  }

});

// route to get complaint attachments
apiRoutes.get('/uploads/:file_id', function (req, res) {    
     var img = fs.readFileSync(__dirname+'/uploads/complaints/'+req.params.file_id);
     res.writeHead(200, {'Content-Type': 'image/jpeg' });
     res.end(img, 'binary');
});

// route to post a complain
// it has an array of images attached
apiRoutes.post('/postComplain', uploadComp.array('images'),function(req, res) {
  var complaint = new Complain();
  // only Student and Faculty user Type can post complains
if (req.decoded._doc.userType == 1 || req.decoded._doc.userType == 2 ) {
  
  var user = req.decoded._doc;
//If not private
  if(complaint.level != '0'){
  complaint.upvotes=[];
  complaint.downvotes=[];
}
  complaint.status='N';
  complaint.tags=req.body.tags;
  complaint.title=req.body.title;
  complaint.description=req.body.description;
  complaint.level=req.body.level;
  complaint.timestamp = Math.floor(Date.now() / 1000);
  complaint.firstName = user.firstName;
  complaint.lastName = user.lastName;
  complaint.dpUrl = user.dpUrl;
  complaint.location = user.location;
  complaint.entryno=user.entryno;
  var usertype;
  var level = req.body.level;
  // resolver usertypes
  if(level=='0') usertype=3;       
  else if(level=='1') usertype=4;       
  else if(level=='2') usertype=5;       
  if(req.files) {
    for (var i = req.files.length - 1; i >= 0; i--) {
    complaint.images.push(req.files[i].filename);
    }
  }
var noti = new Notification({firstName:complaint.firstName,lastName:complaint.lastName,timestamp:complaint.timestamp,type:1,description:complaint.title});
    if(level=='2')
    User.find({userType:1}, function(err, users) {
      for (var i = users.length - 1; i >= 0; i--) {
       if(users[i].entryno!=user.entryno) 
      {users[i].notifications.push(noti);
            users[i].save();}
      };
  });
    else if(level=='1')
    User.find({userType:1,location:user.location}, function(err, users) {
      for (var i = users.length - 1; i >= 0; i--) {
       if(users[i].entryno!=user.entryno) 
        {users[i].notifications.push(noti);
                users[i].save();}
      };
  });
    if(usertype!=5)
    User.findOne({
        location: user.location,
        userType: usertype
            }, function(err, resolver) {
                if(resolver) {
                  resolver.notifications.push(noti);
                  resolver.compReceived=resolver.compReceived+1;
                  resolver.save()
                }
            });
    else 
      User.findOne({
        userType: usertype
            }, function(err, resolver) {
                if(resolver) {
                  resolver.notifications.push(noti);
                  resolver.compReceived=resolver.compReceived+1;
                  resolver.save()
                }
            });

  complaint.save(function(err) {
    if (err) return res.json(err);

    console.log('complaint saved successfully');
    
    res.json({ success: true });

  });
    

}
else 
    res.json({ success: false });
});


// history endpoint fr all the complaints posted by a user or all private complaints in a hostel for the worker user
apiRoutes.get('/history',function(req, res) {
  // if not worker
  if(req.decoded._doc.userType!='3') {
    Complain.find({"entryno":req.decoded._doc.entryno} ,function(err, complaints) {
    for (var i = complaints.length - 1; i >= 0; i--) {
      complaints[i] = JSON.parse(JSON.stringify(complaints[i],function(key,val){
        if(key=='upvotes') return val.length;
        if(key=='downvotes') return val.length;
        else return val;
      }));
    };
    res.json(complaints);
  });}
    // for a worker, find all private complaints in his location
  else 
    { 
    Complain.find({"location":req.decoded._doc.location,"level": "0"} ,function(err, complaints) {

    for (var i = complaints.length - 1; i >= 0; i--) {
      complaints[i] = JSON.parse(JSON.stringify(complaints[i],function(key,val){
        if(key=='upvotes') return val.length;
        if(key=='downvotes') return val.length;
        else return val;
      }));
    };
    res.json(complaints);
  });}
});

// hot institutional level complaints
apiRoutes.get('/insti/hot',function(req, res) {
  Complain.find({"level":"2"} ,function(err, complaints) {
    for (var i = complaints.length - 1; i >= 0; i--) {
      complaints[i] = JSON.parse(JSON.stringify(complaints[i],function(key,val){
        if(key=='upvotes') return val.length;
        if(key=='downvotes') return val.length;
        else return val;
      }));
    };
    res.json(complaints);
  });
});
// trending institutional level complaints
apiRoutes.get('/insti/trending',function(req, res) {
  Complain.find({"level":"2"} ,function(err, complaints) {
    for (var i = complaints.length - 1; i >= 0; i--) {
      complaints[i] = JSON.parse(JSON.stringify(complaints[i],function(key,val){
        if(key=='upvotes') return val.length;
        if(key=='downvotes') return val.length;
        else return val;
      }));
    };res.
    json(complaints);
  });
});
// fresh institutional level complaints
apiRoutes.get('/insti/fresh',function(req, res) {
  Complain.find({"level":"2"} ,function(err, complaints) {
    for (var i = complaints.length - 1; i >= 0; i--) {
      complaints[i] = JSON.parse(JSON.stringify(complaints[i],function(key,val){
        if(key=='upvotes') return val.length;
        if(key=='downvotes') return val.length;
        else return val;
      }));
    };
    res.json(complaints);
  });
});
// hot hostel level complaints
apiRoutes.get('/hostel/hot',function(req, res) {
  Complain.find({"level":"1"} ,function(err, complaints) {
    for (var i = complaints.length - 1; i >= 0; i--) {
      complaints[i] = JSON.parse(JSON.stringify(complaints[i],function(key,val){
        if(key=='upvotes') return val.length;
        if(key=='downvotes') return val.length;
        else return val;
      }));
    };
    res.json(complaints);
  });
});
// trending hostel level complaints
apiRoutes.get('/hostel/trending',function(req, res) {
  Complain.find({"level":"1"} ,function(err, complaints) {
    for (var i = complaints.length - 1; i >= 0; i--) {
      complaints[i] = JSON.parse(JSON.stringify(complaints[i],function(key,val){
        if(key=='upvotes') return val.length;
        if(key=='downvotes') return val.length;
        else return val;
      }));
    };
    res.json(complaints);
  });
});
// fresh hostel level complaints
apiRoutes.get('/hostel/fresh',function(req, res) {
  Complain.find({"level":"1"} ,function(err, complaints) {
    for (var i = complaints.length - 1; i >= 0; i--) {
      complaints[i] = JSON.parse(JSON.stringify(complaints[i],function(key,val){
        if(key=='upvotes') return val.length;
        if(key=='downvotes') return val.length;
        else return val;
      }));
    };
    res.json(complaints);
  });
});

// route to post a comment
apiRoutes.post('/postComment' ,function(req, res) {

  // find the complain
  Complain.findOne({
    _id: req.body.id
  }, function(err, complain) {

    if (err) throw err;

    if (!complain) {
      res.json({ success: false, message: 'complaint not found.' });
    } else if (complain) {
    	// create the comment json
        var user = req.decoded._doc;
        var comment = new Comment({timestamp:Math.floor(Date.now()/1000),entryno:user.entryno,firstName:user.firstName,lastName:user.lastName,body:req.body.body,dpUrl:user.dpUrl});
        // push to complain.comments array
        complain.comments.push(comment);
        complain.save(function(err) {
            if (err)
                return res.send(err);
        // make notifications for all users in the comment section of the complain and the user who posted it
        var notification = new Notification({type:2,timestamp:Math.floor(Date.now()/1000),firstName:user.firstName,lastName:user.lastName,description:req.body.body});
        User.findOne({
            entryno: complain.entryno
              }, function(err, user) {
    				if(user.entryno!=req.decoded._doc.entryno)
                      {
                      	user.notifications.push(notification);
                        user.save();
	                    }
              });
        var userPushed = [];
        for (var i = complain.comments.length - 1; i >= 0; i--) {
            User.findOne({
            entryno: complain.comments[i].entryno
              }, function(err, user) {
                if(userPushed.indexOf(user.entryno)==-1&&(user.entryno!=req.decoded._doc.entryno))
                    {
                      for (var i = user.notifications.length - 1; i >= 0; i--) {
                      };
                      user.notifications.push(notification);
                      user.save();
                      userPushed.push(user.entryno);
                    }
              });
        };

        res.json({
          success: true,
          message: 'comment pushed',
        });
        });
        // return the information including token as JSON
        
      }   


  });
});
// endpoint for upvoting or downvoting a complain
apiRoutes.post('/vote' ,function(req, res) {

  // find the complain
  Complain.findOne({
    _id: req.body.id
  }, function(err, complain) {

    if (err) throw err;

    if (!complain) {
      res.json({ success: false, message: 'complaint not found.' });
    } else if (complain) {

        var entryno = req.decoded._doc.entryno;
        var upvotes = complain.upvotes;
        var downvotes = complain.downvotes;
        var newUpvotes=[],newDownvotes=[];
        var succ = 1;
        var success = true;
        for (var i = upvotes.length - 1; i >= 0; i--) {
          if (upvotes[i] != entryno)
            newUpvotes.push(upvotes[i]);
          else succ = 2;
        }
        for (var i = downvotes.length - 1; i >= 0; i--) {
          if (downvotes[i] != entryno)
            newDownvotes.push(downvotes[i]);
          else succ = 3;
        }
        if(req.body.vote=='1') 
          {
            newUpvotes.push(entryno);
            if(succ==2) success = false;
          }
        else 
        {  
          newDownvotes.push(entryno);
          if(succ==3) success = false;
                }
        complain.upvotes = newUpvotes;
        complain.downvotes = newDownvotes;
        complain.save(function(err) {
            if (err)
                res.send(err);

        // // return the information as JSON
        res.json({
          success: success,
          upvotes: newUpvotes.length,
          downvotes: newDownvotes.length,
          message: 'nyahaha',
        });
        });
        
      }   


  });
});
// endpoint to resolve a complain
apiRoutes.post('/resolve',function(req, res) {

  // find the complain
  Complain.findOne( {
    _id: req.body.id
  }, function(err, complain) {

    if (err) throw err;

    if (!complain) {
      res.json({ success: false, message: 'complaint not found.' });
    } 

    else if(complain.status == 'R') {
      res.json({ success: false, message: 'complaint already resolved.' });
    }

    else {
        var timestamp = Math.floor(Date.now()/1000);
        var user = req.decoded._doc;
        if(user.entryno == complain.entryno &&(user.userType < 3 && user.userType !=0 && complain.level == '0'))
          {
        User.findOne({
        location: user.location,
        userType: 3
            }, function(err, resolver) {
              if (err) 
                return res.json({ success: false, message: 'resolver not found' });
              else {
                if(!resolver) return res.json({ success: false, message: 'resolver not found' });
                var compResolved = resolver.compResolved + 1;
                var avTime;
                if (resolver.avTime&&(resolver.avTime)!=0) avTime = (((timestamp) - complain.timestamp) + resolver.avTime*(compResolved - 1))/compResolved ;  
                else avTime = ((timestamp) - complain.timestamp);
                avTime = Math.floor(avTime);
                resolver.compResolved = compResolved;
                resolver.avTime = avTime;
                complain.status = 'R';
                complain.save();
                resolver.save(function(err) {
                  if (err)
                console.log(err);     
                      // res.send(err); 
               else{
              return res.json({
                success: true,
                message: 'nyahaha',
              });}
              });
              }
        });
        }
        else if(user.userType > 3 && complain.level != '0')
        {
          User.findOne({
        entryno:user.entryno
            }, function(err, resolver) {
              if (err) 
                return res.json({ success: false, message: 'resolver not found' });
              else {
                if(resolver.location) if(resolver.location != complain.location) return res.json({ success: false, message: 'location differ' });
                if(resolver.userType==5&& complain.level != '2') return res.json({ success: false, message: 'not authorized' });
                if(resolver.userType==4&& complain.level != '1') return res.json({ success: false, message: 'not authorized' });
                var compResolved = resolver.compResolved + 1;
                var avTime;
                if (resolver.avTime) avTime = (((timestamp) - complain.timestamp) + resolver.avTime*(compResolved - 1))/compResolved ;  
                else avTime = ((timestamp) - complain.timestamp);
                avTime = Math.floor(avTime);
                resolver.compResolved = compResolved;
                resolver.avTime = avTime;
                complain.status = 'R';
                complain.save();
                resolver.save(function(err) {
                  if (err)
                      res.send(err);
               else{     
              return res.json({
                success: true,
                message: 'nyahaha',
              });}
              });
              }
        });
        }
        else{
        res.json({ success: false, message: 'not authorized' });
      }

    }
  });
});
// route to the profile information of user
apiRoutes.get('/me',function(req,res){
  res.json(req.decoded._doc);
});

// endpoint for all the notifications  for a user
apiRoutes.get('/notifications',function(req,res){
User.findOneAndUpdate({ entryno: req.decoded._doc.entryno }, { notifications:[] }, function(err, user) {
  if (err) throw err;
if(user)return res.status(200).send({ 
        success: true, 
        notifications:user.notifications,
    });
  else return res.status(200).send({ 
        success: false, 
        notifications:'no user',
    });
});
});

app.use('/api', apiRoutes);