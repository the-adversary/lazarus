// app/models.js

var mongoose     = require('mongoose');
var Schema       = mongoose.Schema;
var uniqueValidator = require('mongoose-unique-validator');
var notification = new Schema({      
    type:Number,
    firstName:String,
    lastName:String,
    timestamp:Number,
    description:String
     });
var userSchema = new Schema({
      firstName:  {
        type:String,
        required: true,
    },
    lastName: {
        type:String,
        required: true,
    },
    entryno: {
        type:String,
        required: true,
        unique: true,
        index: true
    },
    location: {
        type:String,
        required: true,
    },
    password: {
        type:String,
        required: true,
    },
    dpUrl : {
        type:String,
        required: true,
    },
    userType: {
        type:Number,
        required: true,
    },
    compReceived: Number,
    compResolved: Number,
    avTime: Number,
    notifications:[notification]
    });
userSchema.plugin(uniqueValidator);

var comment = new Schema({      
        timestamp:{
        type:Number,
        required: true,
    },entryno:{
        type:String,
        required: true,
    },
        firstName:String,
        lastName:String,
        dpUrl:String,
        body: String
    });

var CompSchema = new Schema({
    timestamp:  {
        type:String,
        required: true,
    },
    level:  {
        type:String,
        required: true,
    },
    status:  {
        type:String,
        required: true,
    },
    entryno:  {
        type:String,
        required: true,
    },
    firstName:String,
    lastName:String,
    dpUrl:String,
    tags:[String]
    ,
    location:String,
    description:  {
        type:String,
        required: true,
    },
    title:  {
        type:String,
        required: true,
    },
    upvotes: [String],
    downvotes: [String],
    comments: [comment],
    images:[String]
});



User = mongoose.model('User', userSchema);
Complain = mongoose.model('Complain',CompSchema);
Comment = mongoose.model('Comment',comment);
Notification = mongoose.model('Notification',notification);
exports.User = User;
exports.Notification = Notification;
exports.Complain = Complain;
exports.Comment = Comment;

